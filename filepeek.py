#!/usr/bin/env python

import sys
if len(sys.argv)!=2:
    print('usage: {0} input.root'.format(sys.argv[0]))
    sys.exit(1)

import ROOT

ROOT.xAOD.Init()
fh=ROOT.TFile.Open(sys.argv[1])
tree=ROOT.xAOD.MakeTransientTree(fh,'CollectionTree')
tree.Show(0)
